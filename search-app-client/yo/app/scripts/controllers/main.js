'use strict';

/**
 * @ngdoc function
 * @name yoApp.controller:MainCtrl
 * @description # MainCtrl Controller of the yoApp
 */
angular.module('yoApp').controller('MainCtrl', function($scope, $http) {
	$scope.launchSearch = function(searchInputModel) {
		$scope.searchResult = "No result Found";

		$http({
			method : 'GET',
			url : '/search-app-server-1.0-SNAPSHOT/indexer/search/' + searchInputModel
		}).success(function(data, status, headers, config) {
			$scope.searchResult = "";
			var docs = data.hits.hits;
			for ( var cpt in docs) {
				var item = docs[cpt];
				if (item["_type"] == "doc") {
					var itemFormated = "<p><a href=\"../indexer/content/" + item["_id"] + "\">" + item["fields"]["file.url"] + "</a></p>";
					$scope.searchResult = $scope.searchResult + itemFormated;
				} else {
					$scope.searchResult = $scope.searchResult + " fail ";
				}
			}

		});
	}

});
