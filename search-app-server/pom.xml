<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.invivoo</groupId>
		<artifactId>search-app</artifactId>
		<version>1.0-SNAPSHOT</version>
	</parent>

	<artifactId>search-app-server</artifactId>
	<description>Search application</description>
	<packaging>war</packaging>

	<properties>
		<java.min.version>1.7</java.min.version>
		<maven.min.version>3.0.0</maven.min.version>

		<version.junit>4.11</version.junit>
		<version.javaee_api>7.0</version.javaee_api>

		<version.arquillian_core>1.1.4.Final</version.arquillian_core>

		<version.wildfly>8.0.0.Final</version.wildfly>

		<tika.version>1.7</tika.version>
		<tika.version>1.4</tika.version>

		<deltaspike.version>1.2.1</deltaspike.version>

		<fongo.version>1.6.0</fongo.version>

		<mongo.version>2.12.4</mongo.version>

		<jongo.version>1.1</jongo.version>

		<elasticsearch.version>1.3.9</elasticsearch.version>

		<lucene.version>4.9.1</lucene.version>

	</properties>

	<prerequisites>
		<maven>${maven.min.version}</maven>
	</prerequisites>

	<scm>
		<connection>scm:git:https://gitlab.com/akoita/search-app.git</connection>
		<developerConnection>scm:git:https://somerepository.com/svn_repo/trunk</developerConnection>
		<url>https://gitlab.com/akoita/search-app</url>
	</scm>


	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.jboss.arquillian</groupId>
				<artifactId>arquillian-bom</artifactId>
				<version>${version.arquillian_core}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>com.invivoo</groupId>
			<artifactId>search-app-client</artifactId>
			<type>war</type>
		</dependency>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>${version.javaee_api}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${version.junit}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.arquillian.junit</groupId>
			<artifactId>arquillian-junit-container</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-depchain</artifactId>
			<scope>test</scope>
			<type>pom</type>
		</dependency>
		<!-- Apache Tika -->
		<dependency>
			<groupId>org.apache.tika</groupId>
			<artifactId>tika-core</artifactId>
			<version>${tika.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.tika</groupId>
			<artifactId>tika-parsers</artifactId>
			<version>${tika.version}</version>
		</dependency>
		<!-- Apache Delta Spike -->
		<dependency>
			<groupId>org.apache.deltaspike.core</groupId>
			<artifactId>deltaspike-core-api</artifactId>
			<version>${deltaspike.version}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.deltaspike.core</groupId>
			<artifactId>deltaspike-core-impl</artifactId>
			<version>${deltaspike.version}</version>
			<scope>runtime</scope>
		</dependency>

		<!-- Fongo for MongoDB unit test -->
		<dependency>
			<groupId>com.github.fakemongo</groupId>
			<artifactId>fongo</artifactId>
			<version>${fongo.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- MongoDB Java driver -->
		<dependency>
			<groupId>org.mongodb</groupId>
			<artifactId>mongo-java-driver</artifactId>
			<version>${mongo.version}</version>
		</dependency>

		<!-- Jongo librarry -->
		<dependency>
			<groupId>org.jongo</groupId>
			<artifactId>jongo</artifactId>
			<version>${jongo.version}</version>
		</dependency>

		<!-- Elastic Search -->

		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>${elasticsearch.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-test-framework</artifactId>
			<version>${lucene.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>${elasticsearch.version}</version>
			<scope>test</scope>
			<type>test-jar</type>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>${java.min.version}</source>
					<target>${java.min.version}</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.1.1</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
					<overlays>
						<overlay>
							<groupId>com.invivoo</groupId>
							<artifactId>search-app-client</artifactId>
							<type>war</type>
						</overlay>
					</overlays>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.wildfly.plugins</groupId>
				<artifactId>wildfly-maven-plugin</artifactId>
				<version>1.1.0.Alpha1</version>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<!-- <profile> <id>wildfly-embedded-arquillian</id> <properties> <serverProfile>standalone-full.xml</serverProfile> 
			<serverRoot>${project.build.directory}/wildfly-${version.wildfly}</serverRoot> 
			</properties> <dependencies> <dependency> <groupId>io.undertow</groupId> 
			<artifactId>undertow-websockets-jsr</artifactId> <version>1.0.0.Beta25</version> 
			<scope>test</scope> </dependency> <dependency> <groupId>org.jboss.resteasy</groupId> 
			<artifactId>resteasy-client</artifactId> <version>3.0.5.Final</version> <scope>test</scope> 
			</dependency> <dependency> <groupId>org.jboss.resteasy</groupId> <artifactId>resteasy-jaxb-provider</artifactId> 
			<version>3.0.5.Final</version> <scope>test</scope> </dependency> <dependency> 
			<groupId>org.jboss.resteasy</groupId> <artifactId>resteasy-json-p-provider</artifactId> 
			<version>3.0.5.Final</version> <scope>test</scope> </dependency> <dependency> 
			<groupId>org.wildfly</groupId> <artifactId>wildfly-arquillian-container-embedded</artifactId> 
			<version>${version.wildfly}</version> <scope>test</scope> </dependency> </dependencies> 
			<build> <plugins> <plugin> <artifactId>maven-dependency-plugin</artifactId> 
			<version>2.8</version> <configuration> <skip>${maven.test.skip}</skip> </configuration> 
			<executions> <execution> <id>unpack</id> <phase>process-test-classes</phase> 
			<goals> <goal>unpack</goal> </goals> <configuration> <artifactItems> <artifactItem> 
			<groupId>org.wildfly</groupId> <artifactId>wildfly-dist</artifactId> <version>${version.wildfly}</version> 
			<type>zip</type> <overWrite>false</overWrite> <outputDirectory>${project.build.directory}</outputDirectory> 
			</artifactItem> </artifactItems> </configuration> </execution> </executions> 
			</plugin> <plugin> <artifactId>maven-surefire-plugin</artifactId> <version>2.17</version> 
			<configuration> <systemPropertyVariables> <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager> 
			</systemPropertyVariables> <environmentVariables> <JBOSS_HOME>${project.build.directory}/wildfly-${version.wildfly}</JBOSS_HOME> 
			</environmentVariables> </configuration> </plugin> </plugins> </build> </profile> -->
		<profile>
			<id>wildfly-managed-arquillian</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<serverProfile>standalone-full.xml</serverProfile>
				<serverRoot>${project.build.directory}/wildfly-${version.wildfly}</serverRoot>
			</properties>
			<dependencies>
				<dependency>
					<groupId>io.undertow</groupId>
					<artifactId>undertow-websockets-jsr</artifactId>
					<version>1.0.0.Beta25</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-client</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-jaxb-provider</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-json-p-provider</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.wildfly</groupId>
					<artifactId>wildfly-arquillian-container-managed</artifactId>
					<version>${version.wildfly}</version>
					<scope>test</scope>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-dependency-plugin</artifactId>
						<version>2.8</version>
						<configuration>
							<skip>${maven.test.skip}</skip>
						</configuration>
						<executions>
							<execution>
								<id>unpack</id>
								<phase>process-test-classes</phase>
								<goals>
									<goal>unpack</goal>
								</goals>
								<configuration>
									<artifactItems>
										<artifactItem>
											<groupId>org.wildfly</groupId>
											<artifactId>wildfly-dist</artifactId>
											<version>${version.wildfly}</version>
											<type>zip</type>
											<overWrite>false</overWrite>
											<outputDirectory>${project.build.directory}</outputDirectory>
										</artifactItem>
									</artifactItems>
								</configuration>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<artifactId>maven-surefire-plugin</artifactId>
						<version>2.17</version>
						<configuration>
							<environmentVariables>
								<JBOSS_HOME>${project.build.directory}/wildfly-${version.wildfly}</JBOSS_HOME>
							</environmentVariables>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>wildfly-remote-arquillian</id>
			<dependencies>
				<dependency>
					<groupId>io.undertow</groupId>
					<artifactId>undertow-websockets-jsr</artifactId>
					<version>1.0.0.Beta25</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-client</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-jaxb-provider</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.resteasy</groupId>
					<artifactId>resteasy-json-p-provider</artifactId>
					<version>3.0.5.Final</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.wildfly</groupId>
					<artifactId>wildfly-arquillian-container-remote</artifactId>
					<version>${version.wildfly}</version>
					<scope>test</scope>
				</dependency>
			</dependencies>
		</profile>
		<profile>
			<id>glassfish-embedded-arquillian</id>
			<dependencies>
				<dependency>
					<groupId>org.glassfish.main.extras</groupId>
					<artifactId>glassfish-embedded-all</artifactId>
					<version>4.0</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish</groupId>
					<artifactId>javax.json</artifactId>
					<version>1.0.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.tyrus</groupId>
					<artifactId>tyrus-client</artifactId>
					<version>1.3</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.tyrus</groupId>
					<artifactId>tyrus-container-grizzly-client</artifactId>
					<version>1.3</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.jersey.core</groupId>
					<artifactId>jersey-client</artifactId>
					<version>2.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.arquillian.container</groupId>
					<artifactId>arquillian-glassfish-embedded-3.1</artifactId>
					<version>1.0.0.CR4</version>
					<scope>test</scope>
				</dependency>
			</dependencies>
		</profile>
		<profile>
			<id>glassfish-remote-arquillian</id>
			<dependencies>
				<dependency>
					<groupId>org.glassfish</groupId>
					<artifactId>javax.json</artifactId>
					<version>1.0.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.tyrus</groupId>
					<artifactId>tyrus-client</artifactId>
					<version>1.3</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.tyrus</groupId>
					<artifactId>tyrus-container-grizzly-client</artifactId>
					<version>1.3</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.jersey.core</groupId>
					<artifactId>jersey-client</artifactId>
					<version>2.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.jersey.media</groupId>
					<artifactId>jersey-media-json-jackson</artifactId>
					<version>2.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.glassfish.jersey.media</groupId>
					<artifactId>jersey-media-json-processing</artifactId>
					<version>2.4</version>
					<scope>test</scope>
				</dependency>
				<dependency>
					<groupId>org.jboss.arquillian.container</groupId>
					<artifactId>arquillian-glassfish-remote-3.1</artifactId>
					<version>1.0.0.CR4</version>
					<scope>test</scope>
				</dependency>
			</dependencies>
		</profile>
	</profiles>
</project>
