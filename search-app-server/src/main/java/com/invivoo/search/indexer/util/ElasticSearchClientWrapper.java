package com.invivoo.search.indexer.util;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

import javax.annotation.PreDestroy;
import javax.inject.Singleton;

import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;

@Singleton
public class ElasticSearchClientWrapper {

	private Client client;

	public ElasticSearchClientWrapper() {
		Node node = nodeBuilder().node();
		client = node.client();
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@PreDestroy
	void dispose() {
		client.close();
	}
}
