package com.invivoo.search.service;

import java.io.IOException;
import java.io.OutputStream;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.io.stream.OutputStreamStreamOutput;
import org.elasticsearch.index.get.GetField;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.invivoo.search.indexer.util.ElasticSearchClientWrapper;

@RequestScoped
@Path("")
public class SearchService {
	private static final Logger logger = LoggerFactory.getLogger(SearchService.class);

	@Inject
	private ElasticSearchClientWrapper elasticSearchClientWrapper;

	@GET
	@Path("search/{q}")
	@Produces({ "*" })
	public String search(@PathParam("q") String queryString) {
		logger.info("search queryString = " + queryString);
		Client client = elasticSearchClientWrapper.getClient();
		SearchResponse response = client.prepareSearch("invivoo5").setTypes("doc").setSearchType(SearchType.DFS_QUERY_THEN_FETCH).addField("url")
				.setQuery(QueryBuilders.termQuery("_all", queryString)) // Query
				.addHighlightedField("content").setExplain(true).execute().actionGet();

		logger.info("result = {}", response);
		return response.toString();
	}

	@GET
	@Path("content/{id}")
	@Produces({ "application/pdf" })
	public Response searchContent(@PathParam("id") String docId) {
		logger.info("search queryString = " + docId);
		Client client = elasticSearchClientWrapper.getClient();

		GetResponse response = client.prepareGet("invivoo5", "doc", docId).setFields("attachment").execute().actionGet();
		final GetField contentField = response.getField("attachment");
		Object value = contentField.getValue();

		StreamingOutput stream = new StreamingOutput() {

			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				OutputStreamStreamOutput out = new OutputStreamStreamOutput(output);
				contentField.writeTo(out);
				out.flush();
			}
		};
		//
		// FileOutputStream fos;
		// try {
		// fos = new FileOutputStream("D:\\dev\\search-app\\content.pdf");
		// Base64 decoder = new Base64();
		// byte[] decodedBytes = decoder.
		// ObjectOutputStream oos = new ObjectOutputStream(fos);
		//
		// oos.writeObject(value);
		//
		// oos.close();
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		logger.info("result = {}", response);
		return Response.ok(stream).header("Content-Disposition", "attachment; filename=new-android-book.pdf").build();
	}
}
