package com.invivoo.search.service;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.test.hamcrest.ElasticsearchAssertions.assertHitCount;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.TemplateQueryBuilder;
import org.elasticsearch.test.ElasticsearchIntegrationTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
@ElasticsearchIntegrationTest.ClusterScope(scope = ElasticsearchIntegrationTest.Scope.SUITE)
public class SearchServiceTest extends ElasticsearchIntegrationTest {
	@Before
	public void setup() throws IOException {
		// create an index, make sure it is ready for indexing and add documents
		// to it
		createIndex("invivoo");
		ensureGreen("invivoo");

		index("test", "testtype", "1", jsonBuilder().startObject().field("text", "value1").endObject());
		index("test", "testtype", "2", jsonBuilder().startObject().field("text", "value2").endObject());
		refresh();
	}

	// for our test we want to make sure the config path of the cluster actually
	// points
	// to the test resources that we provide - this is the cluster modification
	// referred
	// to earlier
	@Override
	public Settings nodeSettings(int nodeOrdinal) {
		return ImmutableSettings.settingsBuilder().put(super.nodeSettings(nodeOrdinal)).put("path.conf", this.getResource("config").getPath()).build();
	}

	@Test
	public void testTemplateInBody() throws IOException {
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("template", "all");

		TemplateQueryBuilder builder = new TemplateQueryBuilder("{\"match_{{template}}\": {}}\"", vars);

		// the search client to use in the test comes pre-configured as part of
		// the
		// integration test
		SearchResponse sr = client().prepareSearch().setQuery(builder).execute().actionGet();

		// specific assertions make checks very simple
		assertHitCount(sr, 2);
	}

	@Test
	public void testSearch() {
		fail("Not yet implemented");
	}

	@Test
	public void testSearchContent() {
		fail("Not yet implemented");
	}

}